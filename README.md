# Start
```
export AWS_REGION=us-east-1
sls offline start
```

run lite-server
open http://localhost:4100/frontend/index.html

## To debug local
open another terminal
```
run serverless offline start
```

# Upload problem
open file "generate-form.js"
```
update line 22: `const expiration = new Date(Date.now() + msPerDay * 30).toISOString();//expired in 30 days after today`
```