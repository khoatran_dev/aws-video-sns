app
.service("HomeService", ["$http", "$log", "config", function ($http, $log, config) {
    $log.log("Instantiating HomeService...");
    //console.log('app config: ', config);

    this.processVideo = function (videoKeyName, cId, prefix, fnCallback) {
        //console.log('this.baseURL: ', this.baseUrl);
        $http({
            method: "POST",
            data: JSON.stringify({
                "video": videoKeyName,
                "collectionId": cId,
                "thumbnailPrefix": prefix
            }),
            url: config.String_rtrim(config.apiUrl, '/') + "/processVideo"
        })
        .then(function (res) {
            // success function
            $log.log('createJob', res.data);
            if (typeof fnCallback === 'function')
                fnCallback(res.data);
        },
        function (res) {
            // failure function
            $log.error("ERROR occurred when processing video.");
            if (typeof fnCallback === 'function')
                fnCallback();
        });
    };

    this.waitForJob = function (cId, fnCallback) {
        //console.log('this.baseURL: ', this.baseUrl);
        $http({
            method: "GET",
            url: config.String_rtrim(config.bucketUrl, '/') + "/" + config.String_rtrim(config.uploadDir, '/') + "/" + cId + ".json"
        })
        .then(function (res) {
            // success function
            $log.log('waitForJob', res.data);
            if (typeof fnCallback === 'function')
                fnCallback(res.data);
        },
        function (res) {
            // failure function
            $log.error("ERROR occurred when checking video.");
            if (typeof fnCallback === 'function')
                fnCallback();
        });
    };

    this.searchFaces = function (cId, imgColl, fnCallback) {
        //console.log('this.baseURL: ', this.baseUrl);
        $http({
            method: "POST",
            data: JSON.stringify({
                "collectionId": cId || '',
                "image": imgColl || [],
                "faceMatchThreshold": 90
            }),
            url: config.String_rtrim(config.apiUrl, '/') + "/searchFaces"
        })
        .then(function (res) {
            // success function
            $log.log('searchFaces', res.data);
            if (typeof fnCallback === 'function')
                fnCallback(res.data);
        },
        function (res) {
            // failure function
            $log.error("ERROR occurred when loading process.");
            if (typeof fnCallback === 'function')
                fnCallback();
        });
    };

}])
.controller("HomeController", ['$scope', '$filter', '$q', '$http', '$cookies', 'HomeService', '$timeout', 'config', '$sce', '$compile',
    function ($scope, $filter, $q, $http, $cookies, HomeService, $timeout, config, $sce, $compile) {
        $scope.showVideo = false;
        $scope.isloading = false;
        $scope.imgList = '';
        $scope.uploadMessage = '';
        $scope.uploading = false;
        var timeoutToCheckVideoComplete = 10000;
        var maxErrorCount = 50;
        var currErrorCount = 0;
        var uploaded = [];
        var uniqueCollectionId = '';

        //define template
        var uploadImageTemplate = Handlebars.compile(`<input class="form-control" name="fileimages[]" accept="image/*" multiple="" type="file" id="imageupload">`);
        var uploadVideoTemplate = Handlebars.compile(`<input class="form-control" name="filevideo" accept="video/*" type="file" id="videoupload">`);
        var notallowTemplate = Handlebars.compile(`<li class='list-group-item list-group-item-danger'>File [{{name}} (size: {{size}}) is not allow.
            <span title='Click to cancel upload this file.' style='cursor:pointer' class='badge badge-secondary badge-pill'>x</span></li>`);
        var validTemplate = Handlebars.compile(`
        <li class='list-group-item'>
            <div>
                <a title='Click to start upload' class='lnkupload' href='javascript:void(0);'>{{name}}</a>
                <span title='Click to cancel upload this file.' style='cursor:pointer' class="badge badge-secondary badge-pill"><span class="fa fa-times"></span></span>
            </div>
            <div class="progress" style='display:none;margin-top:10px'>
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" 
                aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
            </div>
        </li>`);
        var uploadedTemplate = Handlebars.compile(`File [{{name}}] has been uploaded.`);
        var imgThumbTemplate = Handlebars.compile(`
        <div class="col-md-3" data-key="{{key}}">
            <div class="card custom-card">
                <div class="text-center d-table">
                    <div class="d-table-cell align-middle"><img class="card-img-top img-fluid" src="{{url}}" alt="{{name}}"></div>
                </div>
                <div class="card-block">
                    <h6 class="card-title">{{name}}</h6>
                    <p class="card-text rek-result"></p>
                </div>
                <!--<ul class="list-group list-group-flush">
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item">Vestibulum at eros</li>
                </ul>
                <div class="card-block">
                    <a href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                </div>-->
            </div>
        </div>
        `);
        var videoTemplate =  Handlebars.compile(`<video id="my-video" controls preload="none" class="video-js vjs-default-skin vjs-big-play-centered" />`);
        //You must need to add boundary conditions, this is just for demonstration
        $scope.to_trusted = function(html_code) {
            return $sce.trustAsHtml(html_code);
        }

        function getFormData($form) {
            var unindexed_array = $form.serializeArray();
            var indexed_array = {};
        
            $.map(unindexed_array, function(n, i){
                indexed_array[n['name']] = n['value'];
            });
        
            return indexed_array;
        }

        function renderImages() {
            if (uploaded.length > 0) {
                //console.log('Start render images');
                var temp = '';
                uploaded.forEach(function(element) {
                    temp += imgThumbTemplate({url: config.String_rtrim(config.bucketUrl, '/') + "/" + element.key, 
                    name: element.name, key: element.key});
                });

                $scope.imgList = temp;
            }
        }

        var lengthOfVideo = 0;
        function checkShowVideo(obj) {
            obj = obj || {};
            //console.log('obj', obj);
            var listFiles = $('#mainform ul.list-group.items li a.lnkupload:visible');
            if (listFiles.length === 0) {
                //All file Is Uploaded
                $timeout(function() {
    
                    if (videojs.getPlayers()['my-video']) {
                        delete videojs.getPlayers()['my-video'];
                    }
                    
                    if (obj.key && obj.name) {
                        //save in cookies
                        $cookies.put('current-video', JSON.stringify(obj));
                        
                        //var videoEl = videoTemplate;
                        $('#mainvideo').find('.videoholder').empty().append(videoTemplate(obj));
                        var player = videojs('my-video', {fluid: true});
                        player.markers({ 
                            /*
                            markerStyle: {
                                'width':'9px',
                                'border-radius': '40%',
                                'background-color': 'orange'
                             },
                             */
                             markerTip: {
                                display: true,
                                text: function(marker) {
                                   return marker.text;
                                }
                             },
                             breakOverlay: {
                                display: false,
                            },
                            markers: []
                        });
            
                        player.one('loadedmetadata', function() {
                            lengthOfVideo = player.duration();
                            //console.log('duration', lengthOfVideo);
                            
                            bindSeek(player);
                        });
                        
                        player.ready(function () {
                            //console.log('Video loaded');
                            player.play();
                            $timeout(() => {
                                player.pause();
                            }, 200);
                        });
                        player.src([
                            {
                                type: "video/mp4", 
                                src: obj.key ? (config.String_rtrim(config.bucketUrl, '/') + "/" + obj.key) : ''
                            },
                            //{ type: "video/webm", src: "http://vjs.zencdn.net/v/oceans.webm" }
                        ]);
                        /* for video thumb
                        player
                        //.dimensions(640, 480)
                        .poster('/frontend/thumb-v2-00020.png');
                        */
                        
                        $scope.showVideo = true;
                        
                        //render list of image uploaded
                        renderImages();
                        $cookies.put('current-images', JSON.stringify(uploaded));
                        
                        $scope.isloading = true;
                        //console.log('$scope.currentCollection.id', $scope.currentCollection.id);
                        
                        if ($scope.showUploadVideo === '2') {
                            currErrorCount = 0;
                            startSearchFaces($scope.currentCollection.id);
                        }
                        else {
                            //create video thumbs
                            var thumbnailPrefix = config.Generate_token(6);
                            if (!uniqueCollectionId) {
                                var prefix = config.Generate_token(6);                    
                                var videoname = obj.name.substring(0, obj.name.lastIndexOf('.'));
                                videoname = _.kebabCase(videoname);
                                var collId = videoname + '_' + prefix;
                                uniqueCollectionId = collId;
        
                                var oldCollections = [];
                                var tempColl = $cookies.get('collection-list');
                                if (tempColl) {
                                    try {
                                        oldCollections = JSON.parse(tempColl);
                                    }
                                    catch(ex) {
                                        oldCollections = [];
                                    }
                                }
            
                                oldCollections.push({id: uniqueCollectionId, name: videoname, video: obj});
                                $cookies.put('collection-list', JSON.stringify(oldCollections));
                                $scope.collectionList.push({id: uniqueCollectionId, name: videoname});
                            }
        
                            HomeService.processVideo(obj.key, uniqueCollectionId, thumbnailPrefix, function(res) {
                                var currVideo = $cookies.get('current-video');
                                if (currVideo && res && !res.errorMessage) {
                                    var obj2 = null;
                                    try {
                                        obj2 = JSON.parse(currVideo);
                                    }
                                    catch(ex) {
                                        obj2 = {};
                                    }

                                    if (res && obj2 && obj2.key && obj2.name)
                                    {
                                        obj2.prefix = thumbnailPrefix;
                                        $cookies.put('current-video', JSON.stringify(obj2));
                                        $timeout(() => {
                                            currErrorCount = 0;
                                            waitJobCompleted();
                                        }, 1000);
                                    }
                                    else
                                        $scope.isloading = false;
                                }
                                else
                                    $scope.isloading = false;
                            });
                        }
                    }
                }, 100);
            }
        }

        function bindSeek() {
            var myPlayer = videojs.getPlayers()['my-video'];
            
            var liColl = $('#mainvideo .atframe li > span');
            if (liColl.length > 0) {
                var n = 0;
                var t = '';
                var temp = [];
                $.each(liColl, function(i,o) {
                    n = parseFloat($(o).text()) || 0;
                    if (n <= lengthOfVideo) {
                        if ($.inArray(n, temp) < 0) {
                            temp.push(n);
                            t = $(o).closest('.card-block').find('.card-title').text();
                            t = $.trim(t);
                            myPlayer.markers.add([{
                                time: n,
                                text: (n/100) + ": " + t
                            }]);
                        }
                    }
                });
            }
            /*
            $timeout(() => {
                var allMarkers = myPlayer.markers.getMarkers();
                console.log(allMarkers);
            }, 4000);
            */

            /*
            // get
            var whereYouAt = myPlayer.currentTime();
            console.log('whereYouAt', whereYouAt);
            // set
            myPlayer.currentTime(5); // 2 minutes into the video
            */
        }

        function waitJobCompleted() {
            var currVideo = $cookies.get('current-video');
            if (currVideo) {
                var obj2 = null;
                try {
                    obj2 = JSON.parse(currVideo);
                }
                catch(ex) {
                    obj2 = {};
                }

                if (obj2)
                {
                    HomeService.waitForJob(uniqueCollectionId, function (res) {
                        var obj = null;
                        if (res) {
                            if (typeof res === 'string') {
                                try {
                                    obj = JSON.parse(res);
                                }
                                catch(ex) {}
                            }
                            else
                                obj = res;
                        }
                            
                        if (obj && obj.status && obj.status.toString() === '2') {
                            console.log('Job complete, start search faces');
                            currErrorCount = 0;
                            startSearchFaces(uniqueCollectionId);
                        }
                        else
                        {
                            if (currErrorCount < maxErrorCount)
                            {
                                currErrorCount += 1;
                                $timeout(function() {
                                    waitJobCompleted();
                                }, timeoutToCheckVideoComplete);
                            }
                            else {
                                console.log('Error occurred or function timeout');
                            }
                        }
                    });
                }
                else
                    $scope.isloading = false;
            }
        }

        function startSearchFaces(cId) {
            var currImages = $cookies.get('current-images');
            var arrImages = [];
            cId = cId || '';
            if (cId.length === 0) {
                var currVideo = $cookies.get('current-video');
                var obj2 = null;
                try {
                    obj2 = JSON.parse(currVideo);
                }
                catch(ex) {
                    obj2 = {};
                }
                
                if(obj2 && Object.keys(obj2).length > 0
                && obj2.collectionId)
                    cId = obj2.collectionId;
            }

            if (currImages) {
                var arr = [];
                try {
                    arr = JSON.parse(currImages);
                }
                catch(ex) {
                    arr = [];
                }
              
                //console.log('startSearchFaces arr', arr);
                if (arr.length > 0) {
                    $.each(arr, function(i, o) {
                        arrImages.push(o.key);
                    });
                    arrImages = _.uniq(arrImages);
                }
                else
                    $scope.isloading = false;
            }

            if (cId && arrImages && arrImages.length > 0) {
                HomeService.searchFaces(cId, arrImages, function(res) {
                    //console.log('Seach result', res);

                    if (res) {
                        var result = res || {};
                        var div = [];
                        var html = '';
                        for (var i = 0; i < arrImages.length; i++) {
                            div = $('div[data-key="' + arrImages[i] + '"]');
                            html = '';
                            if (div.length > 0) {
                                var data = result[arrImages[i]];
                                console.log('arrImages', arrImages, data, result);
                                if (data && data.length > 0) {
                                    for (var n = 0; n < data.length; n++)
                                        html += '<li class="list-inline-item"><span class="badge badge-secondary">' + data[n] + '</span></li>';
                                    div.find('.rek-result').html(`Found at frames:<br/>
                                    <ul class="list-inline atframe">${html}</ul>`);
                                }
                                else
                                {
                                    div.find('.rek-result').html('Not found.');
                                }
                            }
                        }
                        
                        $timeout(() => {
                            bindSeek();
                        }, 50);
                        
                        //clear cookie
                        clearCookies();
                        $scope.isloading = false;
                        //end
                    }
                    else {
                        //limited
                        if (currErrorCount < 5) {
                            currErrorCount += 1;
                            startSearchFaces(cId);
                        }
                        else {
                            currErrorCount = 0;
                            //clear cookie
                            clearCookies();
                            $scope.isloading = false;
                            //end
                        }
                    }
                });
            }
        }
    
        function clearCookies() {
            //clear cookie
            $cookies.remove('current-images');
            //$cookies.remove('current-video');
        }

        var numberFiles = 0;
        var selectedFiles = 0;
        var maxFileToUpload = 0;
        var videoObj = {};
        
        //policy will check uploadDir
        //important: must be same with line 27 ....['starts-with', '$key', 'uploads/'],...
        //file: /generate-form.js
        //important
    
        function startUpload(el) {
            var data = null;
            var liEl = null;
            if ($(el).is('li'))
                liEl = $(el);
            else
                liEl = $(el).closest('li');
            if (liEl.is(':visible')) {
                data = liEl.data('fileuploadapi');
                if (data && liEl.hasClass('uploading') === false) {
                    liEl.addClass('uploading').find('.progress').show();
                    data.submit();
                }
            }
        }

        function initAPI(el, lst, fileType, maxFile, maxSize) {
            fileType = fileType || '';
            fileType = fileType.toLowerCase();
            var options = {
                url: config.bucketUrl,
                autoUpload: false,
                maxFileSize: maxSize,
                maxNumberOfFiles: maxFile,
                sequentialUploads:true,
    
                //important -----
                paramName: 'file',
                //important ----
    
                dataType: 'xml',
                change: function(e, data) {
                    //console.log('Change ', e, data, data.files, selectedFiles);
                    if (fileType === 'image' && maxFileToUpload > 0) {
                        if (selectedFiles < maxFileToUpload) {
                            var n = maxFileToUpload - selectedFiles;
                            if (data.files.length > n)
                                data.files.splice(n);
                        }
                        else
                            data.files = [];
                    }
                },
                progress: function(e, data) {
                    if (data.context) {
                        var per = parseInt(data.loaded / data.total * 100, 10);
                        //console.log('per', per, data.bitrate, data.context);
                        if (per > 100)
                            per = 100;
                        data.context.find('> .progress > .progress-bar').attr('aria-valuenow', per).css('width', per + '%');
                    }
                }
            };
    
            if (fileType === 'image')
                options.acceptFileTypes = /(\.|\/)(gif|jpe?g|png|bmp)$/i;
            else if (fileType === 'video')
                options.acceptFileTypes = /(\.|\/)(avi|flv|mp4|mkv)$/i;
            
            $(el).fileupload(options)
            /*
            .on('fileuploadadd', function (e, data) {
                console.log('check validate', data, data.files.valid);
            })
            */
            .on('fileuploadsubmit', function (e, data) {
                
                // The example input, doesn't have to be part of the upload form:
                var additionData = getFormData($('#mainform'));
                //additionData.type = fileType;
                additionData.key = config.String_rtrim(config.uploadDir, '/') + '/' + data.files[0].name;
                //additionData['Content-Type'] = data.files[0].type;
                data.formData = additionData;
                
                //console.log('fileuploadsubmit data', data);
            })
            .on('fileuploadprocessalways', function (e, data) {
                var currentFile = data.files[data.index];
                var li = null;
                if (data.files.error && currentFile.error) {
                    // there was an error, do something about it
                    //console.log(currentFile.error);
                    //console.log(`File [${currentFile.name} (size: ${currentFile.size}) is not allow.`);
                    li = $(notallow({name: currentFile.name, size: currentFile.size}));
    
                    li.find('.badge').click(function() {
                        //trigger fileuploadfail
                        if (data.abort)
                            data.abort();
    
                        $(this).closest('li').remove();
                        selectedFiles = $('#lstimages').find('a.lnkupload').length;
                    });
    
                    $(lst).append(li);
                    $timeout(function() {
                        li.find('.badge').click();
                    }, 5000);
                }
                else {
                    //console.log(`File [${currentFile.name} (size: ${currentFile.size}) is allow.`);
                    li = $(validTemplate({name: currentFile.name, size: currentFile.size}));
                    data.context = li;
                    li.data('fileuploadapi', data).find('a').click(function() {
                        //disable click
                        //startUpload(this);
                    });
                    //data.submit(); //do submission here
    
                    li.find('.badge').click(function() {
                        if (confirm('Are you sure to cancel this file ?') === true) {
                            //trigger fileuploadfail
                            if (data.abort)
                                data.abort();
                        
                            $(this).closest('li').remove();
                            selectedFiles = $('#lstimages').find('a.lnkupload').length;
                        }
                    });
    
                    if (fileType === 'image')
                        selectedFiles += 1;
                    else if (fileType === 'video')
                        $(lst).empty();

                    $(lst).append(li);
                }
            })
            .on('fileuploaddone', function (e, data) {
                //console.log('data.result.files', data.result.files);
                //console.log('fileuploaddone', e, data, data.xhr(), data.jqXHR, data.jqXHR.getAllResponseHeaders());
                var isSuccess = false;

                if (data.context)
                    data.context.removeClass('uploading');

                if (data.result) {
                    var xmlRes = $(data.result);
                    //console.log('xmlRes', xmlRes, typeof xmlRes, xmlRes.length);
                    if (xmlRes.length > 0) {
                        var el = xmlRes.find('PostResponse Etag');
                        if (el.length > 0)
                            isSuccess = true;
                        el = xmlRes.find('PostResponse Key');

                        if (el.length > 0) {
                            if (fileType === 'image')
                                uploaded.push({key: el.text(), name: data.files[0].name});
                            else
                                videoObj = {key: el.text(), name: data.files[0].name};
                        }

                        //console.log('uploaded', uploaded);
                    }
                }
    
                if (isSuccess === false) {
                    if (data.jqXHR.status === 201 && data.textStatus === 'success')
                        isSuccess = true;
                }
    
                if (isSuccess === true) {
                    data.context.removeClass('list-group-item-warning').addClass('list-group-item-success')
                    .html(uploadedTemplate({name: data.files[0].name}));
                    numberFiles += 1;

                    //console.log('start now');
                    //if (fileType === 'video' && $(lst).children().length > 0)
                        checkShowVideo(videoObj);
                        $scope.uploading = false;
                }
                else
                    data.context.addClass('list-group-item-warning');
            })
            .on('fileuploadfail', function (e, data) {
                //console.log('data.result.files', data.result.files);
                //console.log('fileuploadfail', data);
                
                if (data.context)
                    data.context.removeClass('uploading');

            })/*
            .on('fileuploadcompleted', function (e, data) {
                //console.log('data.result.files', data.result.files);
                console.log('fileuploadcompleted', data);
            })*/;
        }
    
        $scope.doUpload = function() {
            //var api = $('#fileupload').data('blueimpFileupload');
            //console.log('api', api);
            $scope.uploadMessage = '';
            var listimgFiles = $('#lstimages.list-group.items li a.lnkupload:visible');
            if (listimgFiles.length === 0)
            {
                if ($('#lstimages .list-group-item-success').length === 0)
                $scope.uploadMessage = 'Please choose at least a photo then upload.';
                return;
            }

            var listvideoFiles = [];
            if ($scope.showUploadVideo === '1') {
                listvideoFiles = $('#lstvideo.list-group.items li a.lnkupload:visible');
                if (listvideoFiles.length === 0)
                {
                    $scope.uploadMessage = 'Please choose a video then upload.';
                    return;
                }
            }
            else {
                if ($scope.currentCollection.id === '') {
                    $scope.uploadMessage = 'Please select a collection.';
                    return;
                }
            }
           

            $scope.uploading = true;
            listimgFiles.each(function(indx, elLii) {
                //$(this).click();
                startUpload(elLii);
            });

            if (listvideoFiles.length > 0) {
                listvideoFiles.each(function(indx, elLiv) {
                    //$(this).click();
                    startUpload(elLiv);
                });
            }
            else {
                if ($scope.showUploadVideo === '1') {
                    var currVideo = $cookies.get('current-video');
                    //console.log('currVideo', currVideo);
                    if (currVideo) {
                        var obj2 = null;
                        try {
                            obj2 = JSON.parse(currVideo);
                        }
                        catch(ex) {
                            obj2 = {};
                        }
                        videoObj = obj2;
                        //console.log('video obj', videoObj);                    
                    }
                }
                else {
                    var allColl = $cookies.get('collection-list');
                    if (allColl) {
                        var arr = [];
                        try {
                            arr = JSON.parse(allColl);
                        }
                        catch(ex) {
                            arr = [];
                        }
                        
                        if (arr.length > 0) {
                            var cid = $scope.currentCollection.id;
                            console.log('arr', cid, arr);
                            $.each(arr, function(ii, oo) {
                                if (cid === oo.id) {
                                    videoObj = oo.video;
                                    return false;
                                }
                            });

                            console.log('video', videoObj);
                        }
                    }
                }
            }
        };
        
        $scope.showUploadVideo = '1';
        var cookieStr = $cookies.get('collection-list');
        var lstColl = [];
        if (cookieStr) {
            try {
                lstColl = JSON.parse(cookieStr);
            }
            catch(ex) {
                lstColl = [];
            }
        }
        var cookieCollection = [{id: '', name: 'Select a collection'}];
        $.each(lstColl, function(i, o) {
            if (o.id)
                cookieCollection.push(o);
        });        
        $scope.collectionList = cookieCollection;
        $scope.currentCollection = $scope.collectionList[0];
        
        $scope.hideUploadVideo = function(isHide) {
            if (isHide) {
                $('#lstvideo').closest('.form-group').hide();
                $('#videoupload').hide();
                $('#selCol').show();
            }
            else {
                $('#lstvideo').closest('.form-group').show();
                $('#videoupload').show();
                $('#selCol').hide();
            }
        }

        $scope.startAgain = function() {
            $scope.imgList = '';
            uniqueCollectionId = '';
            $scope.uploading = false;
            if (videojs.getPlayers()['my-video']) {
                delete videojs.getPlayers()['my-video'];
            }
            
            $scope.hideUploadVideo(false);
            $scope.currentCollection = $scope.collectionList[0];
            $scope.showUploadVideo = '1';
            //console.log('$scope', $scope);
            uploaded = [];
            clearCookies();
            $('#mainvideo .videoholder, #lstimages, #lstvideo').empty();
            selectedFiles = 0;
            $scope.showVideo = false;
            
            createUploadEl();
            
            initAPI('#imageupload', '#lstimages', 'image', 3, 5000000/* 5MB*/);
            initAPI('#videoupload', '#lstvideo', 'video', 1, 30000000/* 30MB*/);
        
        }
       
        function createUploadEl() {
            $('#imageupload, #videoupload').remove();
            
            var templateEl = angular.element(uploadImageTemplate());
            //Now compile the template with scope $scope
            $compile(templateEl)($scope);
            angular.element('.uploadholder1').append(templateEl);
            
            templateEl = angular.element(uploadVideoTemplate());
            //Now compile the template with scope $scope
            $compile(templateEl)($scope);
            angular.element('.uploadholder2').append(templateEl);
        }

        createUploadEl();

        initAPI('#imageupload', '#lstimages', 'image', 3, 5000000/* 5MB*/, $scope);
        initAPI('#videoupload', '#lstvideo', 'video', 1, 30000000/* 30MB*/);
        
        function startInit() {
            var currImages = $cookies.get('current-images');
            var currVideo = $cookies.get('current-video');
            //console.log(currImages, currVideo);
            if (currImages && currVideo) {
                var arr = [];
                try {
                    arr = JSON.parse(currImages);
                }
                catch(ex) {
                    arr = [];
                }
                var obj2 = null;
                try {
                    obj2 = JSON.parse(currVideo);
                }
                catch(ex) {
                    obj2 = {};
                }

                if (arr.length > 0 && obj2 && Object.keys(obj2).length > 0
                 && obj2.collectionId) {
                    uploaded = arr;
                    checkShowVideo(obj2);
                }
            }
        }

        //startInit();

        /*
        //$scope.showVideo = true;

        //fix to test
        var tempobj = {key: 'uploads/JURASSIC WORLD 2 Official Trailer (2018) Chris Pratt Action Movie HD.mp4', 
        name: 'JURASSIC WORLD 2 Official Trailer (2018) Chris Pratt Action Movie HD.mp4'};
        uploaded = [
        {key: 'Chris Pratt2.jpg', name: 'Chris Pratt2.jpg'},
        {key: 'Chris Pratt.jpg', name: 'Chris Pratt.jpg'},
        {key: 'Bryce Dallas Howard3.jpg', name: 'Bryce Dallas Howard3.jpg'},
        {key: 'Bryce Dallas Howard2.jpg', name: 'Bryce Dallas Howard2.jpg'},
        {key: 'Bryce Dallas Howard.jpg', name: 'Bryce Dallas Howard.jpg'},];
        checkShowVideo(tempobj);
        */
}]);

