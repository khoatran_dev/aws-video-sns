var app = angular.module('Carbon8', ['ngCookies', 'ngSanitize'/*, 'Home', 'Issue'*/]).config(function ($httpProvider) {
    
    //$httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json;charset=utf-8';

    //delete $httpProvider.defaults.headers.common['X-Requested-With'];

    var param = function (obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for (name in obj) {
            value = obj[name];

            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value instanceof Object) {
                for (subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function (data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];

});

app.constant('config', {
    //Note: Must changed
    apiUrl: "https://xmlaayqakc.execute-api.us-east-1.amazonaws.com/dev/",
    //apiUrl: "http://localhost:3000/", 
    bucketUrl: 'https://khoa-test-upload.s3.amazonaws.com',
    uploadDir: 'new-uploads/',
    String_ltrim: function (str, chars) {
        chars = chars || "\\s*";
        return str.replace(new RegExp("^[" + this.RegExpEscape(chars) + "]+", "g"), "");
    },
    String_rtrim: function (str, chars) {
        chars = chars || "\\s*";
        return str.replace(new RegExp("[" + this.RegExpEscape(chars) + "]+$", "g"), "");
    },
    String_trim: function (str, chars) {
        var newstr = this.String_ltrim(str, chars);
        return this.String_rtrim(newstr, chars);
    },
    RegExpEscape: function (s) {
        return String(s).replace(/[\\^$*+?.()|[\]{}]/g, '\\$&');
    },
    Generate_token(length) {
        //edit the token allowed characters
        var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
        var b = [];  
        for (var i=0; i<length; i++) {
            var j = (Math.random() * (a.length-1)).toFixed(0);
            b[i] = a[j];
        }
        return b.join("");
    }

});

    