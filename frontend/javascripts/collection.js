app
.service("HomeService", ["$http", "$log", "config", function ($http, $log, config) {
    $log.log("Instantiating HomeService...");
    //console.log('app config: ', config);

    this.listCollection = function (obj, fnCallback) {
        //console.log('this.baseURL: ', this.baseUrl);
        $http({
            method: "GET",
            params: {
                "thumbnailPrefix": config.Generate_token(6)
            },
            url: config.String_rtrim(config.apiUrl, '/') + "/collections"
        })
        .then(function (res) {
            // success function
            $log.log(res.data);
            if (typeof fnCallback === 'function')
                fnCallback(res.data);
        },
        function (res) {
            // failure function
            $log.error("ERROR occurred when getting collections.");
            if (typeof fnCallback === 'function')
                fnCallback();
        });
    };

    this.deleteCollection = function (obj, fnCallback) {
        //console.log('this.baseURL: ', this.baseUrl);
        $http({
            method: "DELETE",
            data: {
                "cId": obj.cId || ''
            },
            url: config.String_rtrim(config.apiUrl, '/') + "/collection"
        })
        .then(function (res) {
            // success function
            $log.log(res.data);
            if (typeof fnCallback === 'function')
                fnCallback(res.data);
        },
        function (res) {
            // failure function
            $log.error("ERROR occurred when deleting collection " + (obj.cId || '') + ".");
            if (typeof fnCallback === 'function')
                fnCallback();
        });
    };

}])
.controller("HomeController", ['$scope', '$filter', '$q', '$http', '$cookies', 'HomeService', '$timeout',
    function ($scope, $filter, $q, $http, $cookies, HomeService, $timeout) {
        console.log('collection-list in cookie', $cookies.get('collection-list'));
        $scope.getCollection = function() {
            HomeService.listCollection({test:33}, function(res) {
                console.log('res', res);
            });
        }

        $scope.cId = '';

        $scope.deleteCollection = function() {
            if ($scope.cId) {
                HomeService.deleteCollection({cId: $scope.cId}, function(res) {
                    console.log('res', res);
                });
            }
        }

}]);

