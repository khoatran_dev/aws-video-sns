const AWS = require('aws-sdk')
const path = require('path')
var configPath = path.join(__dirname, "aws.json");
//console.log('configPath', configPath);
AWS.config.loadFromPath(configPath);
//console.log('AWS.config', AWS.config);
const Promise = require('bluebird')
const Joi = require('joi')
const _ = require('lodash')
const uuidv1 = require('uuid/v1')
const config = require('./config')
//const helper = require('./helper');
AWS.config.setPromisesDependency(Promise)
const elastictranscoder = new AWS.ElasticTranscoder();
const s3 = new AWS.S3()
const sns = new AWS.SNS();
const rekognition = new AWS.Rekognition();


module.exports.processVideo = (event, context, callback) => {
  console.log('event.body', event.body);
  var body = null;
  try {
    body = JSON.parse(event.body);
  }
  catch(ex) {
    body = null;
  }

  console.log('body', body);
  const schema = Joi.object().keys({
    video: Joi.string().required(),
    thumbnailPrefix: Joi.string().required(),
    collectionId: Joi.string().required()
  })
  const {error} = Joi.validate(body, schema)

  if (error) {
    const response = {
      statusCode: 501,
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Origin' : '*',
      },
      body: JSON.stringify({error: error.details})
    }

    console.log('error: ', error)
    return callback(null, response)
  }

  const { video, thumbnailPrefix, collectionId } = body
  let params = {
    Message: JSON.stringify({'video': video, 'thumbnailPrefix': thumbnailPrefix, 'collectionId': collectionId}),
    TopicArn: `arn:aws:sns:${AWS.config.region}:${config.awsAccountId}:KhoaTestSNSVideo`,
    //arn:aws:sns:us-east-1:130294650733:KhoaTestSNSVideo -> https://console.aws.amazon.com/sns/v2/home?region=us-east-1#/topics/arn:aws:sns:us-east-1:130294650733:KhoaTestSNSVideo
  };

  console.log('Start step 1');
  // Step 1: create key status
  let fileKey = `${collectionId}.json`;
  
  createBucketKey(fileKey, function() {
    sns.publish(params, (error) => {
      if (error) {
        console.error(error);
        callback(null, {
          statusCode: 501,
          headers: { 
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Origin' : '*',
            'Content-Type': 'text/plain' },
          body: 'Error when processing video.',
        });
        return;
      }

      // create a resonse
      const response = {
        statusCode: 200,
        headers: { 
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
          'Content-Type': 'text/plain' },
        body: JSON.stringify({ message: 'Video has been processed successfully!', 'params': params }),
      };

      callback(null, response);
    });
  });
}

// create job to generate thumb image
module.exports.snsVideo = (event, context, callback) => {
  console.log('event.Records', event.Records);
  var body = null;
  try {
    body = JSON.parse(event.Records[0].Sns.Message);
  }
  catch(ex) {
    body = null;
  }
  
  const schema = Joi.object().keys({
    video: Joi.string().required(),
    thumbnailPrefix: Joi.string().required(),
    collectionId: Joi.string().required()
  })
  const {error} = Joi.validate(body, schema)

  if (error) {
    const response = {
      statusCode: 501,
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Origin' : '*',
      },
      body: JSON.stringify({error: error.details})
    }
    console.log('error: ', error)
    return callback(null, response)
  }

  const { video, thumbnailPrefix, collectionId } = body

  const params = {
    PipelineId: config.PIPELINE_ID,
    /* required */
    Input: {
      Key: video
    },
    Output: {
      Key: uuidv1() + path.extname(video),
      PresetId: config.PRESET_ID,
      ThumbnailPattern: `${thumbnailPrefix}-{count}`
    },
    OutputKeyPrefix: config.outputKeyPrefix
  }

  // Step 2: create job
  elastictranscoder.createJob(params, function (err, data) {
    if (err) {
      console.log(err, err.stack) // an error occurred
      callback(null, {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify({msg: err.stack})
      });
      return;
    }    
    else {

      console.log('Start step 3', data.Job.Id);
      // Step 3: wait for job complete
      // reset recursive wait job
      curErrorWait = 0;
      startWaitJob({ body: JSON.stringify({ Id: data.Job.Id }) }, {}, function() {
        
        console.log('Start step 4');
        let fileKey = `${collectionId}.json`;
        // 4: index faces
        startIndexFaces({ body: JSON.stringify({
          'thumbnailPrefix': thumbnailPrefix,
          'collectionId': collectionId,
          'keyfile': fileKey,
          }) }, {}, function() {
          updateStatusKey(fileKey, '2', function() {
            console.log('video process complete');
          })
        });

      });

    }
  });
}

var maxErrorWait = 5;
var curErrorWait = 0;
function startWaitJob(event, context, callback) {
    console.log('startWaitJob', event.body);
  
    const body = JSON.parse(event.body)
    if (!body || !body.Id) {
      console.log('No data to processing...');
      callback(null, {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify({msg: 'Please provide [Id].'})
      });
      return;
    }
  
    const params = {
      Id: body.Id
    }

    try {
      elastictranscoder.waitFor('jobComplete', params, function (err, data) {
        if (err) {
          console.log('Error wait job', err.stack)
        } else {
          console.log('jobComplete', data)
          if (typeof callback === 'function')
            callback();
        }
      });
    }
    catch(ex) {
      console.log('Error timeout', ex);

      if (curErrorWait < maxErrorWait)
      {
        curErrorWait += 1;
        startWaitJob(event, context, callback);
      }
    }
}

// wait for job completed
module.exports.waitJob = startWaitJob;

// check job status
module.exports.readJob = (event, context, callback) => {
  const body = JSON.parse(event.body)
  
  if (!body || !body.Id) {
    console.log('No data to processing...');
    callback(null, {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Origin' : '*',
      },
      body: JSON.stringify({msg: 'Please provide [Id].'})
    });
    return;
  }

  const params = {
    Id: body.Id
  }

  elastictranscoder.readJob(params, function (err, data) {
    if (err) {
      console.log(err, err.stack)
      callback(null, {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify({msg: err.stack})
      });
      return;
    } else {
      const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify(data)
      }
      callback(null, response)
    }
  })
}

// list Jobs By Status
module.exports.listJobsByStatus = (event, context, callback) => {
  const body = JSON.parse(event.body)
  
  if (!body || !body.status) {
    console.log('No data to processing...');
    callback(null, {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Origin' : '*',
      },
      body: JSON.stringify({msg: 'Please provide [status].'})
    });
    return;
  }

  var params = {
    Status: body.status, /* required */
    Ascending: body.asc || false,
    PageToken: body.pageToken
  };

  elastictranscoder.listJobsByStatus(params, function (err, data) {
    if (err) {
      console.log(err, err.stack)
      callback(null, {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify({msg: err.stack})
      });
      return;
    } else {
      const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify(data)
      }
      callback(null, response)
    }
  })
}

function startIndexFaces(event, context, callback) {
  console.log('startIndexFaces', event.body);
  const body = JSON.parse(event.body)
  const {
    thumbnailPrefix,
    collectionId,
    keyfile
  } = body

  const defaultParams = {
    Bucket: config.bucketName,
    Prefix: config.outputKeyPrefix + thumbnailPrefix
  }
  const params = defaultParams

  const s3object = s3.listObjects(params).promise()

  s3object.then(function (res) {
    var params = {
      CollectionId: collectionId
    }
    return rekognition.createCollection(params).promise().then(data => {
      return {
        CollectionId: params.CollectionId,
        Images: res.Contents
      }
    })
  })
    .then(res => {
      //console.log('res', res)
      if (res && res.Images) {
        const allIndexFacesPromises = Promise.map(res.Images, (item, index) => {
          var params = {
            CollectionId: collectionId,
            ExternalImageId: `${index + 1}`,
            Image: {
              S3Object: {
                Bucket: config.bucketName,
                Name: item.Key
              }
            }
          }
          return rekognition.indexFaces(params).promise()
        })
        return Promise.all(allIndexFacesPromises)
      }
    })
    .then(data => {
      if (typeof callback === 'function')
        callback();
    })
    .catch(function (err) {
      console.log('Error', err.stack)
    })
}

module.exports.indexFaces = startIndexFaces;


function updateStatusKey(keyfile, statusMsg, callback) {
  
    var params = {
        Bucket: config.bucketName,
        Key: config.uploadDir + keyfile,
        Body: JSON.stringify({ status: statusMsg }),
        ACL: 'public-read',
    };
  
    console.log('updateStatusKey params', params);
    try {
        s3.putObject(params, function (perr, pres) {
            console.log('perr', perr, 'pres', pres);
            if (perr) {
                console.log('Error update file', perr);
            }
            else {            
              console.log('Key updated successful', pres);
            }
  
            if (typeof callback === 'function')
              callback();
        });  
    }
    catch(ex) {
        console.log('upload error ', ex);
        if (typeof callback === 'function')
          callback();
    }
  
}
  
function createBucketKey(keyfile, callback) {
  
    var params = {
        Bucket: config.bucketName,
        Key: config.uploadDir + keyfile,
        Body: JSON.stringify({status: '1'}),
        ACL: 'public-read',
    };
  
    console.log('createBucketKey params', params);
    try {
        s3.putObject(params, function (perr, pres) {
            console.log('perr', perr, 'pres', pres);
            if (perr) {
                console.log('Error add file', perr);
            }
            else {            
              console.log('Key created successful', pres);
              if (typeof callback === 'function')
                callback();
            }
        });
    }
    catch(ex) {
        console.log('upload error ', ex);
    }
  
}
  