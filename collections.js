const AWS = require('aws-sdk')
const path = require('path')
var configPath = path.join(__dirname, "aws.json");
//console.log('configPath', configPath);
AWS.config.loadFromPath(configPath);
//console.log('AWS.config', AWS.config);
const Promise = require('bluebird')
AWS.config.setPromisesDependency(Promise)
const _ = require('lodash')
const config = require('./config')
const rekognition = new AWS.Rekognition()
//const helper = require('./helper');

module.exports.create = (event, context, callback) => {

}

module.exports.list = (event, context, callback) => {
  const body = JSON.parse(event.body)
  /*
  if (!body) {
    console.log('No data to processing...');
    callback(null, {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Origin' : '*',
      },
      body: JSON.stringify({msg: 'No data to processing.'})
    });
    return;
  }
  */
  rekognition.listCollections(body, function (err, res) {
    if (err) 
    {
      console.log(err, err.stack) // an error occurred
      callback(null, {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify({msg: err.stack})
      });
      return;
    }
    else {
      const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify(res)
      }

      callback(null, response)
    }
  })
}

module.exports.delete = (event, context, callback) => {
  const body = JSON.parse(event.body)
  
  if (!body || !body.cId) {
    console.log('No data to processing...');
    callback(null, {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Origin' : '*',
      },
      body: JSON.stringify({msg: 'Please provide [cId].'})
    });
    return;
  }

   rekognition.deleteCollection({CollectionId: body.cId}, function(err, data) {
     if (err) {
        console.log('Error', err.stack); // an error occurred
        callback(null, {
          statusCode: 500,
          headers: {
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Origin' : '*',
          },
          body: JSON.stringify({msg: err.stack})
        });
        return;
     }
     else {
       console.log('Success', data);           // successful response
       const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify(data)
      }

      callback(null, response)
    }
   });
}
