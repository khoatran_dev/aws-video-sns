const AWS = require('aws-sdk'),
path = require('path'),
fs = require('fs');
const config = require('./config')
var configPath = path.join(__dirname, "aws.json");
//console.log('configPath', configPath);
AWS.config.loadFromPath(configPath);
//console.log('AWS.config', AWS.config);
const s3 = new AWS.S3();

var Generate_token = function(length) {
    //edit the token allowed characters
    var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
    var b = [];  
    for (var i=0; i<length; i++) {
        var j = (Math.random() * (a.length-1)).toFixed(0);
        b[i] = a[j];
    }
    return b.join("");
}

var params = {
    Bucket: config.bucketName,
    Key: 'test-key-' + Generate_token(6) + '.json',
    Body: JSON.stringify({status: 'Just been updated on ' + (new Date())}),
    ACL: 'public-read',
};

//console.log('params', params);
try {
    s3.putObject(params, function (perr, pres) {
        console.log('perr', perr, 'pres', pres);
        if (perr) {
            console.log('Error add file', perr);
            
            var response1 = {
            "statusCode": 200,
            "headers": {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin' : '*',
                "my_header": "my_value"
            },
            "body": JSON.stringify(perr),
            //"isBase64Encoded": false
            };
    
            callback(null, response1);
        }
        else {            
            var response2 = {
            "statusCode": 200,
            "headers": {
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Origin' : '*',
                "my_header": "my_value"
            },
            "body": JSON.stringify(pres || {msg: 'Nothing to response >>>'}),
            //"isBase64Encoded": false
            };
    
            callback(null, response2);
        }
    });  
}
catch(ex) {
    console.log('upload error ', ex);
    return Promise.reject({});
}


function callback(action, res) {
    console.log('Call back', res);
}
