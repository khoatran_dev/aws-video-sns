#!/usr/bin/env node

const crypto = require('crypto');

const AWS = require('aws-sdk'),
fs = require('fs'),
path = require('path');

const config = require('./config')


var configPath = path.join(__dirname, "aws.json");
//console.log('configPath', configPath);
AWS.config.loadFromPath(configPath);
//console.log('AWS.config',  AWS.config.credentials.accessKeyId);
//console.log('AWS.config',  AWS.config.credentials.secretAccessKey);

const awsAccessKeyId = AWS.config.credentials.accessKeyId;
const awsSecretAccessKey = AWS.config.credentials.secretAccessKey;

const msPerDay = 24 * 60 * 60 * 1000;
const expiration = new Date(Date.now() + msPerDay * 30).toISOString();//expired in 30 days after today
const bucketUrl = `https://${config.bucketName}.s3.amazonaws.com`;

const policy = {
  expiration,
  conditions: [
    ['starts-with', '$key', config.uploadDir],
    { bucket: config.bucketName },
    { acl: 'public-read' },
    //['starts-with', '$Content-Type', 'image/'],
    //['starts-with', '$Content-Type', 'video/'],
    { success_action_status: '201' },
  ],
};

const policyB64 = Buffer(JSON.stringify(policy), 'utf-8').toString('base64');

const hmac = crypto.createHmac('sha1', awsSecretAccessKey);
hmac.update(new Buffer(policyB64, 'utf-8'));

const signature = hmac.digest('base64');

fs.readFile('frontend/index.template.html', 'utf8', (err, input) => {
  if (err) {
    console.log(err);
  }

  const data = input
    .replace(/%BUCKET_URL%/g, bucketUrl)
    .replace(/%AWS_ACCESS_KEY%/g, awsAccessKeyId)
    .replace(/%POLICY_BASE64%/g, policyB64)
    .replace(/%SIGNATURE%/g, signature);

  fs.writeFile('frontend/index.html', data, 'utf8', (e) => {
    if (e) {
      console.log(e);
    }
  });
});
