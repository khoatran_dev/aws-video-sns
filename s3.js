const AWS = require('aws-sdk')
const path = require('path')
var configPath = path.join(__dirname, "aws.json");
//console.log('configPath', configPath);
AWS.config.loadFromPath(configPath);
//console.log('AWS.config', AWS.config);
const Promise = require('bluebird')
AWS.config.setPromisesDependency(Promise)
const _ = require('lodash')
const s3 = new AWS.S3()
const config = require('./config')
//const helper = require('./helper');

module.exports.listObject = (event, context, callback) => {
  const body = JSON.parse(event.body)
  const {thumbnailPrefix} = body
  if (!body || !thumbnailPrefix) {
    console.log('No data to processing...');
    callback(null, {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Origin' : '*',
      },
      body: JSON.stringify({msg: 'Please provide [thumbnailPrefix].'})
    });
    return;
  }

  const params = {
    Bucket: config.bucketName,
    Prefix: config.outputKeyPrefix + thumbnailPrefix
  }

  s3.listObjects(params, function (err, res) {
    if (err) {
      console.log(err, err.stack) // an error occurred
      callback(null, {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify({msg: err.stack})
      });
    }
    else {
      const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify(res)
      }

      callback(null, response)
    }
  })
}
