const AWS = require('aws-sdk')
const path = require('path')
var configPath = path.join(__dirname, "aws.json");
//console.log('configPath', configPath);
AWS.config.loadFromPath(configPath);
//console.log('AWS.config', AWS.config);
const Promise = require('bluebird')
AWS.config.setPromisesDependency(Promise)
const _ = require('lodash')
const rekognition = new AWS.Rekognition()

const config = require('./config')

module.exports.searchFaces = (event, context, callback) => {
  const body = JSON.parse(event.body)
  const {
    collectionId,
    image,
    faceMatchThreshold
  } = body
  const params = {
    CollectionId: collectionId,
    FaceMatchThreshold: faceMatchThreshold || 97,
    Image: {
      S3Object: {
        Bucket: config.bucketName,
        Name: image
      }
    }
  }
  rekognition.searchFacesByImage(params, function (err, data) {
    if (err) console.log(err, err.stack) // an error occurred
    else {
      const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify(data)
      }
      callback(null, response)
    }
  })
}

module.exports.searchFacesByMultipleImages = (event, context, callback) => {
  const body = JSON.parse(event.body)
  let {
    collectionId,
    image,
    faceMatchThreshold
  } = body

  if (!_.isArray(image)) {
    image = [image]
  }
  const allSearchPromises = Promise.map(image, (item, index) => {
    const params = {
      CollectionId: collectionId,
      FaceMatchThreshold: faceMatchThreshold || 97,
      Image: {
        S3Object: {
          Bucket: config.bucketName,
          Name: item
        }
      }
    }
    return rekognition.searchFacesByImage(params).promise()
  })
  Promise.all(allSearchPromises).then(facesRes => {
    let facesByImages = {}
    image.forEach((item, index) => {
      facesByImages[item] = faceMatchesToFrame(facesRes[index].FaceMatches)
    })
    return facesByImages
  })
    .then(result => {
      const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify(result)
      }
      callback(null, response)
    })
    .catch(error => {
      const response = {
        statusCode: 501,
        headers: {
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify({
          error: error
        })
      }
      callback(null, response)
    })
}

/**
 *
 * @param {array} faceMatches
 */
const faceMatchesToFrame = (faceMatches) => {
  if (!faceMatches || (faceMatches.length === 0)) {
    return []
  }
  faceMatches = _.sortBy(faceMatches, [(item) => {
    return item.Face.ExternalImageId
  }])
  faceMatches = _.map(faceMatches, (item, index) => {
    return item.Face.ExternalImageId
  })
  return faceMatches
}